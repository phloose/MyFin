const path = require("path");

const testdir = path.resolve(__dirname, "__tests__/");
const snapshots = path.resolve(__dirname, "__snapshots__/");

module.exports = {
    testPathForConsistencyCheck: "__tests__/example.spec.js",

    /** resolves from test to snapshot path */
    resolveSnapshotPath: (testPath, snapshotExtension) => {
        return testPath.replace(testdir, snapshots) + snapshotExtension;
    },

    /** resolves from snapshot to test path */
    resolveTestPath: (snapshotFilePath, snapshotExtension) => {
        return snapshotFilePath
            .replace(snapshots, testdir)
            .slice(0, -snapshotExtension.length);
    },
};
