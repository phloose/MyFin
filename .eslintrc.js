/* eslint-disable global-require */
module.exports = {
    parser: "@typescript-eslint/parser",
    plugins: ["svelte3", "@typescript-eslint"],
    extends: ["phloose/ts", "prettier"],
    settings: {
        "svelte3/typescript": require("typescript"),
    },
    overrides: [
        {
            files: ["**/*.svelte"],
            processor: "svelte3/svelte3",
        },
    ],
};
