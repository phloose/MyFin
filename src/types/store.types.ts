import type { Writable } from "svelte/store";

export interface IToast {
    content?: string;
    style?: string;
}

export interface IToastStore extends Writable<IToast> {
    show: (text: string, style?: string) => void;
    reset: () => void;
}
