import type { Member } from "@app/types/member.type";
import type {
    ITransaction,
    IMemberTransaction,
} from "@app/types/transaction.types";

export class MemberTransaction implements IMemberTransaction, Member {
    id: string;

    name: string;

    amount: number;

    constructor(id: string, name: string, amount: number) {
        this.id = id;
        this.name = name;
        this.amount = amount;
    }
}

export class Transaction implements ITransaction {
    readonly id: string;

    date: string;

    currency: string;

    title: string;

    credits: Array<MemberTransaction>;

    debits: Array<MemberTransaction>;

    constructor(
        id: string,
        date: string,
        currency: string,
        title: string,
        credits: Array<MemberTransaction>,
        debits: Array<MemberTransaction>,
    ) {
        this.id = id;
        this.date = date;
        this.currency = currency;
        this.title = title;
        this.credits = credits;
        this.debits = debits;
    }

    public get total(): number {
        let sum = 0;
        this.credits.forEach(e => {
            sum += e.amount;
        });
        return sum;
    }

    // Calculate the impact this transaction has on a members overall balance
    impactOnMember(id: string): number {
        let creditTotal = 0;
        this.credits.forEach(e => {
            if (e.id === id) {
                creditTotal += e.amount;
            }
        });

        let debitTotal = 0;
        this.debits.forEach(e => {
            if (e.id === id) {
                debitTotal += e.amount;
            }
        });

        const impactTotal: number = debitTotal - creditTotal;
        return impactTotal;
    }
}
