// Type Imports
import type { RxJsonSchema } from "rxdb";
import type {
    ITransaction,
    IMemberTransaction,
} from "../types/transaction.types";

const MemberTransactionSchema: RxJsonSchema<IMemberTransaction> = {
    title: "Member transaction",
    description: "A record of a member making a transaction",
    version: 0,
    type: "object",
    properties: {
        id: {
            ref: "members",
            type: "string",
        },
        // Amount in CENTS
        amount: {
            type: "integer",
        },
    },
};

export const TransactionSchema: RxJsonSchema<ITransaction> = {
    title: "transaction",
    description:
        "A record of a transaction with debited and credited participants",
    version: 0,
    type: "object",
    properties: {
        id: {
            type: "string",
            primary: true,
            final: true,
        },
        date: {
            type: "string",
        },
        title: {
            type: "string",
        },
        currency: {
            type: "string",
        },
        credits: {
            type: "array",
            minItems: 1,
            items: {
                type: "object",
                properties: MemberTransactionSchema.properties,
            },
        },
        debits: {
            type: "array",
            minItems: 1,
            items: {
                type: "object",
                properties: MemberTransactionSchema.properties,
            },
        },
    },
    required: ["id", "date", "currency", "title", "credits", "debits"],
    encrypted: ["title", "currency", "credits", "debits"],
};
