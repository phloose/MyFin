module.exports = {
    transform: {
        "^.+\\.svelte$": [
            "svelte-jester",
            {
                preprocess: true,
            },
        ],
        "^.+\\.ts$": "ts-jest",
    },
    moduleFileExtensions: ["js", "ts", "svelte"],
    moduleDirectories: ["node_modules", "src"],
    setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
    snapshotResolver: "./snapshotResolver.js",
};
